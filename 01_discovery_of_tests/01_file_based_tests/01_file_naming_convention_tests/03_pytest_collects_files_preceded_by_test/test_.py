# Tests (generically speaking) are simply assertions that what you expect
# and what you get are the same
#
# There are 5 "test" (not pytest) functions in this file, verifiable by running
#
# python test.py
#
# You should see "5 Functions containing the word 'test' Found"
#
# However, running:
#
# py.test
#
# displays '0 valid pytest items found'
#
# Why is that?
#
# We can force pytest to run this test file explicitly by running:
# py.test

# You should see "5 Functions containing the word 'test' Found"

# Now you should also see '1 Valid pytest items found'
#
# Why is only 1 test function found? -- NAMING CONVENTIONS! =)
#
# Valid pytest items are functions begin with the word test_
#
# pytest collects valid test items (functions) functions from files with valid
# naming conventions and runs each in sequence
#
# Valid test collection points include files explicitly passed into py.test -- (AKA) py.test <FILENAME>
# Valid test collection points include files preceded by test_
# Valid test collection points include files followed by _test
#
# You can verify this behavior by running py.test from this directory
#
# You should see '1 Valid pytest Test Items Found
# You should also see '1 Tests Expected'

expected_tests = 1
functions_containing_the_word_test_found = 0

def test():
    assert(1==1)

functions_containing_the_word_test_found += 1

def anothertest():
    assert(1==1)

functions_containing_the_word_test_found += 1

def Test():
    assert(1==1)

functions_containing_the_word_test_found += 1

def _test():
   assert(1==1)

functions_containing_the_word_test_found += 1

def _test_():
    assert(1==1)

functions_containing_the_word_test_found += 1

if __name__ == '__main__':

    test()
    anothertest()
    Test()
    _test()
    _test_
    print functions_containing_the_word_test_found, "Functions containing the word 'test' Found"

import pytest

@pytest.fixture(scope="session", autouse=True)
def expected(request):
    print expected_tests, "Tests expected"

