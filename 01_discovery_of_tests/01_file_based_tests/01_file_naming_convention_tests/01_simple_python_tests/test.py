# Tests (generically speaking) are simply assertions that what you expect
# and what you get are the same
#
# There are 5 "test" (not pytest) functions in this file, verifiable by running
#
# python test.py
#
# You should see "5 Functions containing the word 'test' Found"
#
# However, running:
#
# py.test prints '0 valid pytest items found'
#
# Why is that?

functions_containing_the_word_test_found = 0

def test():
    assert(1==1)

functions_containing_the_word_test_found += 1

def anothertest():
    assert(1==1)

functions_containing_the_word_test_found += 1

def Test():
    assert(1==1)

functions_containing_the_word_test_found += 1

def _test():
    assert(1==1)

functions_containing_the_word_test_found += 1

def _test_():
    assert(1==1)

functions_containing_the_word_test_found += 1

import pytest

if __name__ == '__main__':

    test()
    anothertest()
    Test()
    _test()
    _test_
    print functions_containing_the_word_test_found, "Functions containing the word 'test' Found"

