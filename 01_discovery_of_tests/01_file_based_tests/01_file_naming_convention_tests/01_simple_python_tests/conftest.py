import pytest

@pytest.fixture(scope='session',autouse=True)
def print_collector_items(request):
    def pytest_collectreport(report):
        print len(report.__dict__.items()), 'Valid pytest Test Items Found'
        print report
