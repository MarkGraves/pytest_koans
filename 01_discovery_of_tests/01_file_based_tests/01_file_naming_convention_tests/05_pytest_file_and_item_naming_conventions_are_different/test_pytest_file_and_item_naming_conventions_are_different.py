# Tests (generically speaking) are simply assertions that what you expect
# and what you get are the same
#
# Valid test collection points include files preceded by test_
# Valid test collection points include files followed by _test
# Valid test collection points include files explicitly passed into py.test
# (AKA) py.test <FILENAME>
#
# pytest collects valid test files
# Then, inside each it collects valid test items (functions)
#
# Valid pytest items are functions begin with the word test
#
# Naming conventions for test files and test items are different
#


expected_tests = 1
functions_containing_the_word_test_found = 0


def test_():
    '''
        This test is a valid pytest item
    '''
    assert(1 == 1)

functions_containing_the_word_test_found += 1


def not_a_test():
    '''
        This test is not a valid pytest item
    '''
    assert(1 == 1)

functions_containing_the_word_test_found += 1

import pytest

@pytest.fixture(scope="session", autouse=True)
def expected(request):
    print expected_tests, "Tests expected"


if __name__ == '__main__':
    test_()
    not_a_test()
    print functions_containing_the_word_test_found, "Functions containing the word 'test' Found"



