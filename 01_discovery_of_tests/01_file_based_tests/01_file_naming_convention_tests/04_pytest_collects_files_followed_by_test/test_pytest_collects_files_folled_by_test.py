# Tests (generically speaking) are simply assertions that what you expect
# and what you get are the same
#
# Valid pytest items are functions begin with the word test
#
# pytest collects valid test items (functions) functions from files with valid
# naming conventions and runs each in sequence
#
# Test files preceded by test_ are valid test collection points
# Test files followed by _test are valid test collection points
#
# You can verify this behavior by running py.test from this directory
#
# You should see '1 Valid pytest Test Items Found
# You should also see '1 Tests Expected'
#

expected_tests = 1

def test():
    assert(1==1)

import pytest

@pytest.fixture(scope="session", autouse=True)
def expected(request):
    print expected_tests, "Tests expected"

