# You can mix functions and methods defined in test classes

# There are two tests to be discovered in this file
# The first function is a valid test because it is preceded by test_
# The class also serves as a valid test collection point
# and the test_ prefixed method gets picked up as a valid test item

def test_a_function_preceded_by_test_in_file():
    '''
        This test will be run
            It IS a valid test:
            -The function name is preceded by keyword test (GOOD)
            --> This convention is not the same for file discovery
    '''
    assert('will run' == 'will run')

def a_function_followed_by_test_is_not_a_test():
    '''
        This test will NOT be run
            It is NOT a valid test:
            -The function name is followed by keyword test (BAD)
            --> This convention is not the same for file discovery
    '''
    assert('wont run' == 'will run')

class TestClassWithNoInitMethodInFileWithFunctionTests:
    def test_method_preceded_by_test_keyword_is_a_test(self):
        '''
            This test will be run
            It IS a valid test:
            -The classname is preceded by upper case Test (GOOD)
            -The class has no init method (GOOD)
            -The method name is preceded by keyword test (GOOD)
            --> This convention is not the same for file discovery
        '''
        assert('will run' == 'will run')