#
# Classes PRECEDED by UPPER CASE Test ARE test classes
# which serve as collection nodes for test items
#
# Classes FOLLOWED by EITHER CASE test are NOT test classes.
# They don't serve as collection nodes even if
# test methods are defined properly in the class
#
# Classes WITH an init method are NOT test classes
# They don't serve as collection nodes even if
# test methods are defined properly in the class
#
## Classes with NO init method ARE test classes
# which serve as collection nodes for test items
#
# Within a class based test discovery node,
# methods prefixed with test_ keyword ARE valid tests
# methods followed by _test are NOT valid tests
#
# As such there is only 1 valid test in this file.
# The first two are not collected by pytest due to the naming of the class
# The second test method in the only valid test class does not run
# because it is followed by _test, instead of prefixed with test_
#

class ClassesFollwedByUpperCaseTestKeywordDontWorkAsCollectionPointsTest:
    def test_classes_can_not_be_followed_by_test_keyword(self):
        '''
            This test will not be run
            It is NOT a valid test:
            -The classname is followed by upper case Test (BAD)
            -The class has no init method (GOOD)
            -The method name is preceded by keyword test (GOOD)
            --> This convention is not the same for file discovery
        '''
        assert('wont run' == 'will run')

class testClassesPrecededByLowerCasetestDontWorkAsCollectionPoints:
    def test_classes_can_not_be_preceded_by_lower_case_test_keyword(self):
        '''
            This test will not be run
            It is NOT a valid test:
            -The classname is preceded by lower case test (BAD)
            -The class has no init method (GOOD)
            -The method name is preceded by keyword test (GOOD)
            --> This convention is not the same for file discovery
        '''
        assert('wont run' == 'will run')


class TestClassesMustbePrefixedWithUpperCaseTestKeyword:
    def test_classes_must_be_prefixed_with_upper_case_test_keyword(self):
        '''
            This test will pass
            It is a valid test:
            -The classname is prefixed by upper case Test (Good)
            -The class has no init method (GOOD)
            -The method name is preceded by keyword test (GOOD)
        '''
        assert('will run' == 'will run')

    def class_methods_followed_by_test_keyword_is_not_a_test(self):
        '''
            This test will not be run
            It is NOT a valid test:
            -The classname is prefixed by upper case Test (Good)
            -The class has no init method (GOOD)
            -The method name is followed by keyword test (BAD)
            --> This convention is not the same for file discovery
        '''
        assert('wont run' == 'will run')


