#
# Classes PRECEDED by UPPER CASE Test ARE test classes
# which serve as collection nodes for test items
#
# Classes FOLLOWED by EITHER CASE test are NOT test classes.
# They don't serve as collection nodes even if
# test methods are defined properly in the class
#
# Classes WITH an init method are NOT test classes
# They don't serve as collection nodes even if
# test methods are defined properly in the class
#
## Classes with NO init method ARE test classes
# which serve as collection nodes for test items
#
# Within a class based test discovery node,
# methods prefixed with test_ keyword ARE valid tests
# methods followed by _test are NOT valid tests
#
# As such there is only 1 valid test in this file.
#
# The first class does not serve as a test collection point because
# it has an init method
#
# The second test method in the only valid test class does not run
# because it is followed by _test, instead of preceded by test_
#

class TestClassWithInitMethod:
    def __init__(self):
        pass
    def test_method_preceded_by_test_keyword_is_a_test(self):
        '''
            This test will not be run.
            It is NOT a valid test:
            -The classname is preceded by upper case Test (GOOD)
            -The class has an init method (BAD)
            -The method name is preceded by keyword test (GOOD)
            --> This convention is not the same for file discovery
        '''
        assert('wont run' == 'will run')

class TestClassWithNoInitMethod:

    def test_method_preceded_by_test_keyword_is_a_test(self):
        '''
            This test will be run
            It IS a valid test:
            -The classname is preceded by upper case Test (GOOD)
            -The class has no init method (GOOD)
            -The method name is preceded by keyword test (GOOD)
            --> This convention is not the same for file discovery
        '''
        assert('will run' == 'will run')


    def method_followed_by_test_keyword_is_not_a_test(self):
        '''
            This test will not be run.
            It is NOT a valid test:
            -The classname is preceded by upper case Test (GOOD)
            -The class has no init method (GOOD)
            -The method name is followed by keyword test (BAD)
            --> This convention is not the same for file discovery
        '''
        assert('will run' == 'wont run')


