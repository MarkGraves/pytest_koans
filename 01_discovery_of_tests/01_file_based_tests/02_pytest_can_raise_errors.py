#
# test files preceded by test_ are valid test collection points
# test files followed by _test are valid test collection points
#
# functions preceded by test_ are valid test items
# functions followed by _test are NOT valid test items
#
# As such, there is one valid test to be discovered in this file (the one
# preceded by test_ keyword.

import pytest

expected_tests =2

def test_function_preceded_by_test_in_valid_test_collection_point():
    '''
        This test will be run
            It IS a valid test:
            -The function name is preceded by keyword test (GOOD)
            --> This convention is not the same for file discovery
    '''
    assert('will run' == 'will run')

def test_functions_can_expect_errors():
    with pytest.raises(ZeroDivisionError):
        1/0

def function_followed_by_test_in_valid_test_collection_point_is_not_a_test():
    '''
        This test will NOT be run
            It is NOT a valid test:
            -The function name is followed by keyword test (BAD)
            --> This convention is not the same for file discovery
    '''
    assert('wont run' == 'will run')

@pytest.fixture(scope="session", autouse=True)
def expected(request):
    print expected_tests, 'Tests Expected'