# functions preceded by test_ are valid test items
# functions followed by _test are NOT valid test items
#
# As such, there is one valid test to be discovered in this file (the one
# preceded by test_ keyword.
#
# Running this file from outside this directory will still find this test item
# This behavior can be disabled by setting norecursedirs in pytest.ini, tox.ini, or setup.cfg

def test():
    '''
        This test will be run
        It IS a valid test:
            -The function name is preceded by keyword test (GOOD)
            --> This convention is not the same for file discovery
            - This test is discovered even if run from other directories
              because pytest recurses into directories
    '''

    assert('will run' == 'will run')