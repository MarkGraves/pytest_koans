#
# Fixtures are functions
#
# Because they are also standard python, they must at least contain a pass
# statement in order to compile. They do not have to return anything in
# order to be a fixture
#
# Fixture functions must be invoked in order to access them.
#
# Fixtures get called automatically when they are passed as function arguments
# This is dependency injection
#
# Calling the fixture as an argument to the test function invokes the fixture
#
# Fixtures take a scope parameter for which the fixture is SHARED
#
# One of ['function','class','module','session']
#
# Fixtures scoped as functions are re-invoked across each test item
# Fixtures scoped as class are shared across all test methods in a test class
# Fixtures scoped as module are shared across all test methods in a file
# Fixtures scoped as session are shared across tests in multiple files
#
# In order to use session scoped fixtures across files, they must be defined in scope of
# the test file (ie using conftest or other more advanced methods)
#
# As such, no tests are able to run if this file is run directly
#
# (ie, but not explicitly an entire pytest testing session -- have to check
# the docs to determine if you can have multiple session, for example: running
# an automated testing script from the command line -- probably advanced)
#
# Fixture functions can be parameterized
# just like other functions, with iterables
#
# Fixture params are passed in as an iterable (list, dict, tuple, etc)
#
# The fixture function gets access to each parameter
# through the special request object
#
# Fixture functions can be parameterized in which case they will be
# called multiple times, each time executing the set of dependent tests,
# i. e. the tests that depend on this fixture.
#
# ALL TESTS USING PARAMS WILL BE RUN ONCE FOR EACH ITEM IN PARAMS
#

import pytest

@pytest.fixture(params=['1','2'])
def list_parameterized_fixture(request):
    print request.param
    pass

@pytest.fixture(params=('1','2'))
def tuple_parameterized_fixture(request):
    print request.param
    pass

@pytest.fixture(params={'1':'2', '3':'4'})
def dict_parameterized_fixture(request):
    print request.param
    pass

def test_list_parameterized_function(list_parameterized_fixture):
    pass

def test_dict_paramaterized_function(dict_parameterized_fixture):
    pass

def test_tuple_paramaterized_function(tuple_parameterized_fixture):
    pass


