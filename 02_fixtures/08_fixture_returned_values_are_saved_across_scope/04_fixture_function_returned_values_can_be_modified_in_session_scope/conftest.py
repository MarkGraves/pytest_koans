import pytest

@pytest.fixture(scope='session')
def session_scoped_fixture():

    return {'output':1}
