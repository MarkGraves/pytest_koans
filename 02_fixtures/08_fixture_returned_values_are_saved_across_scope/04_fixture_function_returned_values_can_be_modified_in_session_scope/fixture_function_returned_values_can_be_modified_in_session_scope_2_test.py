# Fixtures take a scope parameter for which the fixture is SHARED
#
# One of ['function','class','module','session']
#
# Fixtures scoped as functions are re-invoked across each test item
# Fixtures scoped as class are shared across all test methods in a test class
# Fixtures scoped as module are shared across all test methods in a file
# Fixtures scoped as session are shared across tests in multiple files
#
# In order to use session scoped fixtures across files, they must be defined in scope of
# the test file (ie using conftest or other more advanced methods)
#
# As such, no tests are able to run if this file is run directly
#
# (ie, but not explicitly an entire pytest testing session -- have to check
# the docs to determine if you can have multiple session, for example: running
# an automated testing script from the command line -- probably advanced)


def test_session_scoped_fixtures_share_across_functions_3(session_scoped_fixture):
    print session_scoped_fixture['output']
    assert(session_scoped_fixture['output'] == 5)

    session_scoped_fixture['output'] = 6

    print session_scoped_fixture['output']
    assert(session_scoped_fixture['output'] == 6)

def test_session_scoped_fixtures_share_across_functions_4(session_scoped_fixture):
    print session_scoped_fixture['output']
    assert(session_scoped_fixture['output'] == 6)

    session_scoped_fixture['output'] = 7

    print session_scoped_fixture['output']
    assert(session_scoped_fixture['output'] == 7)

class Test_session_scoped_fixtures_share_across_classes_3:

    def test_session_scoped_fixture_shared_across_class_in_another_file_1(self, session_scoped_fixture):
        print session_scoped_fixture['output']
        assert(session_scoped_fixture['output'] == 7)

        session_scoped_fixture['output'] = 8

        print session_scoped_fixture['output']
        assert(session_scoped_fixture['output'] == 8)

class Test_session_scoped_fixtures_share_across_classes_4:
    def test_session_scoped_fixture_shared_across_class_in_another_file_2(self, session_scoped_fixture):
        print session_scoped_fixture['output']
        assert(session_scoped_fixture['output'] == 8)

        session_scoped_fixture['output'] = 9

        print session_scoped_fixture['output']
        assert(session_scoped_fixture['output'] == 9)
