# Fixtures take a scope parameter for which the fixture is SHARED
#
# One of ['function','class','module','session']
#
# Fixtures scoped as functions are re-invoked across each test item
# Fixtures scoped as class are shared across all test methods in a test class
# Fixtures scoped as module are shared across all test methods in a file
# Fixtures scoped as session are shared across tests in multiple files
#
# In order to use session scoped fixtures across files, they must be defined in scope of
# the test file (ie using conftest or other more advanced methods)
#
# As such, no tests are able to run if this file is run directly
#
# CAVEAT - (ie, but not explicitly an entire pytest testing session -- have to check
# the docs to determine if you can have multiple session, for example: running
# an automated testing script from the command line -- probably advanced)




def test_session_scoped_fixtures_share_across_functions_1(session_scoped_fixture):
    print session_scoped_fixture['output']
    assert(session_scoped_fixture['output'] == 1)

    session_scoped_fixture['output'] = 2

    print session_scoped_fixture['output']
    assert(session_scoped_fixture['output'] == 2)

def test_session_scoped_fixtures_share_across_functions_2(session_scoped_fixture):
    print session_scoped_fixture['output']
    assert(session_scoped_fixture['output'] == 2)

    session_scoped_fixture['output'] = 3

    print session_scoped_fixture['output']
    assert(session_scoped_fixture['output'] == 3)


class Test_session_scoped_fixtures_share_across_classes_1:

    def test_session_scoped_fixture_shared_across_class_in_file_1(self, session_scoped_fixture):
        print session_scoped_fixture['output']
        assert(session_scoped_fixture['output'] == 3)

        session_scoped_fixture['output'] = 4

        print session_scoped_fixture['output']
        assert(session_scoped_fixture['output'] == 4)


class Test_session_scoped_fixtures_share_across_classes_2:
    def test_session_scoped_fixture_shared_across_class_in_file_2(self, session_scoped_fixture):
        print session_scoped_fixture['output']
        assert(session_scoped_fixture['output'] == 4)

        session_scoped_fixture['output'] = 5

        print session_scoped_fixture['output']
        assert(session_scoped_fixture['output'] == 5)

