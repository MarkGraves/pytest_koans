#
# Fixtures are functions
#
# Because they are also standard python, they must at least contain a pass
# statement in order to compile. They do not have to return anything in
# order to be a fixture
#
# Fixture functions must be invoked in order to access them.
#
# Fixtures get called automatically when they are passed as function arguments
# This is dependency injection
#
# Calling the fixture as an argument to the test function invokes the fixture
#
# Fixtures take a scope parameter for which the fixture is SHARED
#
# One of ['function','class','module','session']
#
# Fixtures scoped as functions are re-invoked across each test item
# Fixtures scoped as class are shared across all test methods in a test class
# Fixtures scoped as module are shared across all test methods in a file
# Fixtures scoped as session are shared across tests in multiple files
#
# In order to use session scoped fixtures across files, they must be defined in scope of
# the test file (ie using conftest or other more advanced methods)
#
# As such, no tests are able to run if this file is run directly
#
# (ie, but not explicitly an entire pytest testing session -- have to check
# the docs to determine if you can have multiple session, for example: running
# an automated testing script from the command line -- probably advanced)
#
# Fixture functions can be parameterized
# just like other functions, with iterables
#
# Fixture params are passed in as an iterable (list, dict, tuple, etc)
#
# The fixture function gets access to each parameter
# through the special request object
#
# Fixture functions can be parameterized in which case they will be
# called multiple times, each time executing the set of dependent tests,
# i. e. the tests that depend on this fixture.
#
# ALL TESTS USING PARAMS WILL BE RUN ONCE FOR EACH ITEM IN PARAMS
#
#
# Because fixtures are functions, they can return things.
#
# We can access those returned values the same way you would anything
# returned from a python function (string, dict, list, class, etc)
#
# Special care should be considered in returning classes.
# One instance is not the same as another instance
#
# Because returned values are python objects, they can be modified
# with standard python
#
# Fixtures take a scope parameter for which the fixture is SHARED
# One of ['function','class','module','session']
#
# Fixtures scoped as functions are re-invoked across each test item
#
# Fixtures scoped as class are shared across all test methods in a test class
#
# Fixtures scoped as module are shared across all test methods in a file


import pytest

@pytest.fixture(scope='module')
def module_scoped_fixture():

    return {'output':1}

def test_module_scoped_fixtures_share_across_functions_1(module_scoped_fixture):
    assert(module_scoped_fixture['output'] == 1)
    module_scoped_fixture['output'] = 2
    assert(module_scoped_fixture['output'] == 2)


def test_module_scoped_fixtures_share_across_functions_2(module_scoped_fixture):
    assert(module_scoped_fixture['output'] == 2)
    module_scoped_fixture['output'] = 3
    assert(module_scoped_fixture['output'] == 3)

class Test_module_scoped_fixtures_share_across_classes_1:

    def test_module_scoped_fixture_shared_across_class_in_file_1(self, module_scoped_fixture):
        assert(module_scoped_fixture['output'] == 3)
        module_scoped_fixture['output'] = 4
        assert(module_scoped_fixture['output'] == 4)

class Test_module_scoped_fixtures_share_across_classes_2:
    def test_module_scoped_fixture_shared_across_class_in_file_2(self, module_scoped_fixture):
        assert(module_scoped_fixture['output'] == 4)
        module_scoped_fixture['output'] = 5
        assert(module_scoped_fixture['output'] == 5)
