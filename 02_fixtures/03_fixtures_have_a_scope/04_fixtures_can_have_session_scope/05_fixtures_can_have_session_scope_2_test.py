#
# Fixtures are functions
#
# Because they are also standard python, they must at least contain a pass
# statement in order to compile. They do not have to return anything in
# order to be a fixture
#
# Fixture functions must be invoked in order to access them.
#
# Fixtures get called automatically when they are passed as function arguments
# This is dependency injection
#
# Calling the fixture as an argument to the test function invokes the fixture
#
# Fixtures take a scope parameter for which the fixture is SHARED
#
# One of ['function','class','module','session']
#
# Fixtures scoped as functions are re-invoked across each test item
# Fixtures scoped as class are shared across all test methods in a test class
# Fixtures scoped as module are shared across all test methods in a file
# Fixtures scoped as session are shared across tests in multiple files
#
# In order to use session scoped fixtures across files, they must be defined in scope of
# the test file (ie using conftest or other more advanced methods)
#
# As such, no tests are able to run if this file is run directly
#
# (ie, but not explicitly an entire pytest testing session -- have to check
# the docs to determine if you can have multiple session, for example: running
# an automated testing script from the command line -- probably advanced)


import os

this_file_path =  os.path.abspath(__file__)
this_directory = os.path.dirname(this_file_path)

output_file_1 = os.path.join(this_directory,'session_scoped_fixture_output.txt')

def test_session_scoped_fixtures_share_across_functions_3(session_scoped_fixture):
    with open(output_file_1,'rb') as file:
        fixture_output_1 = int(file.read())
        assert(fixture_output_1 == session_scoped_fixture)

def test_session_scoped_fixtures_share_across_functions_4(session_scoped_fixture):
    with open(output_file_1,'rb') as file:
        fixture_output_1 = int(file.read())
        assert(fixture_output_1 == session_scoped_fixture)

class Test_session_scoped_fixtures_share_across_classes_3:

    def test_session_scoped_fixture_shared_across_class_in_another_file_1(self, session_scoped_fixture):
        with open(output_file_1,'rb') as file:
            fixture_output_1 = int(file.read())
            assert(fixture_output_1 == session_scoped_fixture)

class Test_session_scoped_fixtures_share_across_classes_4:
    def test_session_scoped_fixture_shared_across_class_in_another_file_2(self, session_scoped_fixture):
        with open(output_file_1,'rb') as file:
            fixture_output_1 = int(file.read())
            assert(fixture_output_1 == session_scoped_fixture)

