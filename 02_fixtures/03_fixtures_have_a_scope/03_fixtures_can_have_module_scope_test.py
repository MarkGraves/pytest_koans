#
# Fixtures are functions
#
# Because they are also standard python, they must at least contain a pass
# statement in order to compile. They do not have to return anything in
# order to be a fixture
#
# Fixture functions must be invoked in order to access them.
#
# Fixtures get called automatically when they are passed as function arguments
# This is dependency injection
#
# Calling the fixture as an argument to the test function invokes the fixture
#
# Fixtures take a scope parameter for which the fixture is SHARED
# One of ['function','class','module','session']
#
# Fixtures scoped as functions are re-invoked across each test item
#
# Fixtures scoped as class are shared across all test methods in a test class
#
# Fixtures scoped as module are shared across all test methods in a file


import pytest
import random
import os

this_file_path =  os.path.abspath(__file__)
this_directory = os.path.dirname(this_file_path)

output_file_1 = os.path.join(this_directory,'module_scoped_fixture_output.txt')

@pytest.fixture(scope='module')
def module_scoped_fixture():
    fixture_output = random.randint(0,1000)

    print fixture_output

    return fixture_output

def test_module_scoped_fixtures_share_across_functions_1(module_scoped_fixture):

    with open(output_file_1,'w') as file:
        file.write(str(module_scoped_fixture))

    with open(output_file_1,'rb') as file:
        fixture_output_1 = int(file.read())
        assert(fixture_output_1 == module_scoped_fixture)

def test_module_scoped_fixtures_share_across_functions_2(module_scoped_fixture):

    fixture_output_2 = module_scoped_fixture

    with open(output_file_1,'rb') as file:
        fixture_output_1 = int(file.read())
        assert(fixture_output_1 == fixture_output_2)

class Test_module_scoped_fixtures_share_across_classes_1:

    def test_module_scoped_fixture_shared_across_class_in_file_1(self, module_scoped_fixture):

        fixture_output_3 = module_scoped_fixture

        with open(output_file_1,'rb') as file:
            fixture_output_1 = int(file.read())
            assert(fixture_output_1 == fixture_output_3)

class Test_module_scoped_fixtures_share_across_classes_2:
    def test_module_scoped_fixture_shared_across_class_in_file_2(self, module_scoped_fixture):

        fixture_output_4 = module_scoped_fixture

        with open(output_file_1,'rb') as file:
            fixture_output_1 = int(file.read())
            assert(fixture_output_1 == fixture_output_4)