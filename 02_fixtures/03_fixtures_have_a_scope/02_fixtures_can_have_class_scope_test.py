#
# Fixtures are functions
#
# Because they are also standard python, they must at least contain a pass
# statement in order to compile. They do not have to return anything in
# order to be a fixture
#
# Fixture functions must be invoked in order to access them.
#
# Fixtures get called automatically when they are passed as function arguments
# This is dependency injection
#
# Calling the fixture as an argument to the test function invokes the fixture
#
# Fixtures take a scope parameter for which the fixture is SHARED
# One of ['function','class','module','session']
#
# Fixtures scoped as functions are re-invoked across each test item
#
# Fixtures scoped as class are shared across all test methods in a test class

import pytest
import random
import os

this_file_path =  os.path.abspath(__file__)
this_directory = os.path.dirname(this_file_path)

output_file_1 = os.path.join(this_directory,'class_scoped_fixture_output.txt')

@pytest.fixture(scope='class')
def class_scoped_fixture():
    fixture_output = random.randint(0,1000)

    print fixture_output

    return fixture_output

class Test_fixtures_scoped_as_class_are_shared_across_its_methods:

    def test_class_scoped_fixture_1(self,class_scoped_fixture):
        with open(output_file_1,'w') as file:
            file.write(str(class_scoped_fixture))
        with open(output_file_1,'rb') as file:
            fixture_output_1 = int(file.read())
            assert(fixture_output_1 == class_scoped_fixture)

    def test_class_scoped_fixture_2(self, class_scoped_fixture):
        fixture_output_2 = class_scoped_fixture
        with open(output_file_1,'rb') as file:
            fixture_output_1 = int(file.read())
            assert(fixture_output_1 == fixture_output_2)

class Test_fixtures_scoped_as_class_only_share_across_a_single_class:
    def test_class_scoped_fixture_3(self, class_scoped_fixture):
        fixture_output_3 = class_scoped_fixture
        with open(output_file_1,'rb') as file:
            fixture_output_1 = int(file.read())
            assert(fixture_output_1 != fixture_output_3)
