#
# Fixtures are functions
#
# Because they are also standard python, they must at least contain a pass
# statement in order to compile. They do not have to return anything in
# order to be a fixture
#
# Fixture functions must be invoked in order to access them.
#
# There are no tests in this file (invoking the function and thus printing),
# so you will see no printed output
#
import pytest

@pytest.fixture
def this_is_the_simplest_pytest_fixture():
    print 'You will not see Hello World'
    pass
