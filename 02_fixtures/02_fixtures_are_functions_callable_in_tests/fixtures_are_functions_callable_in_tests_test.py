#
# Fixtures are functions
#
# Because they are also standard python, they must at least contain a pass
# statement in order to compile. They do not have to return anything in
# order to be a fixture
#
# Fixture functions must be invoked in order to access them.
#
# Fixtures get called automatically when they are passed as function arguments
# This is dependency injection
#
# Calling the fixture as an argument to the test function invokes the fixture
# Thus, you see 'Hello World' when this file is called via py.test -s
#
# Although there are two fixtures in this file, and two print statements
# Only one is called in a test function
# Thus, you don't see 'Goodbye World'
#


import pytest

@pytest.fixture()
def fixture_called_in_a_test():
    print 'Hello World'

@pytest.fixture()
def uncalled_fixture():
    print 'Goodbye World'

def test_calling_a_fixture(fixture_called_in_a_test):
    pass
