#
# Fixtures are functions
#
# Because they are also standard python, they must at least contain a pass
# statement in order to compile. They do not have to return anything in
# order to be a fixture
#
# Fixture functions must be invoked in order to access them.
#
# Fixtures get called automatically when they are passed as function arguments
# This is dependency injection
#
# Calling the fixture as an argument to the test function invokes the fixture
#
# Fixtures take a scope parameter for which the fixture is SHARED
#
# One of ['function','class','module','session']
#
# Fixtures scoped as functions are re-invoked across each test item
# Fixtures scoped as class are shared across all test methods in a test class
# Fixtures scoped as module are shared across all test methods in a file
# Fixtures scoped as session are shared across tests in multiple files
#
# In order to use session scoped fixtures across files, they must be defined in scope of
# the test file (ie using conftest or other more advanced methods)
#
# As such, no tests are able to run if this file is run directly
#
# (ie, but not explicitly an entire pytest testing session -- have to check
# the docs to determine if you can have multiple session, for example: running
# an automated testing script from the command line -- probably advanced)
#
# Fixture functions can be parameterized
# just like other functions, with iterables
#
# Fixture params are passed in as an iterable (list, dict, tuple, etc)
#
# The fixture function gets access to each parameter
# through the special request object
#
# Fixture functions can be parameterized in which case they will be
# called multiple times, each time executing the set of dependent tests,
# i. e. the tests that depend on this fixture.
#
# ALL TESTS USING PARAMS WILL BE RUN ONCE FOR EACH ITEM IN PARAMS
#
#
# Because fixtures are functions, they can return things.
#
# We can access those returned values the same way you would anything
# returned from a python function (string, dict, list, class, etc)
#
# Special care should be considered in returning classes.
# One instance is not the same as another instance
#
# REMEMBER TO USE py.test -s to see print values!

import pytest
import inspect


@pytest.fixture()
def string_fixture():
    return 'Hello World'

@pytest.fixture()
def dictionary_fixture():
    return {'returned_value':'Hello World'}


@pytest.fixture()
def list_fixture():
    return ['Hello World']


class class_fixture:
    def __init__(self):
        self.message = 'Hello World'
    pass

@pytest.fixture()
def class_fixture_function():
    return class_fixture()

def test_fixture_returning_a_string(string_fixture):
    print string_fixture

    assert(string_fixture == 'Hello World')
    assert(isinstance(string_fixture, str))

def test_fixture_returning_a_dict(dictionary_fixture):
    print dictionary_fixture['returned_value']

    assert(dictionary_fixture['returned_value'] == 'Hello World')
    assert(isinstance(dictionary_fixture,dict))


def test_fixture_returning_a_list(list_fixture):
    print list_fixture

    assert(list_fixture == ['Hello World'])
    assert(isinstance(list_fixture,list))

def test_fixture_returning_a_class(class_fixture_function):
    print class_fixture_function

    assert(inspect.isclass(class_fixture_function),True)
    assert(isinstance(class_fixture_function,class_fixture))
    assert(class_fixture_function.message == 'Hello World')

def test_class_instations_are_not_the_same(class_fixture_function):
    print class_fixture_function

    assert(inspect.isclass(class_fixture_function),True)
    assert(isinstance(class_fixture_function,class_fixture))
    assert(class_fixture_function.message == 'Hello World')

    another_instance = class_fixture()

    print another_instance

    assert(inspect.isclass(another_instance),True)
    assert(isinstance(another_instance,class_fixture))
    assert(another_instance.message == 'Hello World')

    assert(class_fixture_function != another_instance)


