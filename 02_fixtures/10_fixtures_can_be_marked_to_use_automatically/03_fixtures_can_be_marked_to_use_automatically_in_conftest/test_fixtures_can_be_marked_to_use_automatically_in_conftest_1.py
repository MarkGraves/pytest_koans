#
# Fixtures are functions
#
# Because they are also standard python, they must at least contain a pass
# statement in order to compile. They do not have to return anything in
# order to be a fixture
#
# Fixture functions must be invoked in order to access them.
#
# Fixtures get called automatically when they are passed as function arguments
# This is dependency injection
#
# Calling the fixture as an argument to the test function invokes the fixture
#
# Fixtures take a scope parameter for which the fixture is SHARED
#
# One of ['function','class','module','session']
#
# Fixtures scoped as functions are re-invoked across each test item
# Fixtures scoped as class are shared across all test methods in a test class
# Fixtures scoped as module are shared across all test methods in a file
# Fixtures scoped as session are shared across tests in multiple files
#
# In order to use session scoped fixtures across files, they must be defined in scope of
# the test file (ie using conftest or other more advanced methods)
#
# As such, no tests are able to run if this file is run directly
#
# (ie, but not explicitly an entire pytest testing session -- have to check
# the docs to determine if you can have multiple session, for example: running
# an automated testing script from the command line -- probably advanced)
#
# Fixture functions can be parameterized
# just like other functions, with iterables
#
# Fixture params are passed in as an iterable (list, dict, tuple, etc)
#
# The fixture function gets access to each parameter
# through the special request object
#
# Fixture functions can be parameterized in which case they will be
# called multiple times, each time executing the set of dependent tests,
# i. e. the tests that depend on this fixture.
#
# ALL TESTS USING PARAMS WILL BE RUN ONCE FOR EACH ITEM IN PARAMS
#
#
# Because fixtures are functions, they can return things.
#
# We can access those returned values the same way you would anything
# returned from a python function (string, dict, list, class, etc)
#
# Special care should be considered in returning classes.
# One instance is not the same as another instance
#
# Because returned values are python objects, they can be modified
# with standard python
#
# Fixtures scoped as functions are re-invoked across each test item
# Fixtures scoped as class are shared across all test methods in a test class
# Fixtures scoped as module are shared across all test methods in a file
# Fixtures scoped as session are shared across tests in multiple files
#
# Sometimes test functions do not directly need access to a fixture object.
# in that case you can mark a test item with
#
# @pytest.mark.usefixtures('FIXTURE_FUNCTION_NAME')
#
# Due to the usefixtures marker, the fixture called by name will be required
# for the execution of each test method,
#
# However, you will not have access to its values unless it is passed in
#
# Occasionally, you may want to have fixtures get
# invoked automatically without a usefixtures or funcargs reference.

# Autouse applies at a scope level:
#
# if an autouse fixture is defined in a test module,
# all its test functions automatically use it.
#
# if an autouse fixture is defined in a test class,
# all test methods in the class will use this fixture without a need to
# state it in the test function signature or with a class-level usefixtures decorator.
#
# if an autouse fixture is defined in a conftest.py file then all tests
# in all test modules belows its directory will invoke the fixture.
#
# Notice the terminology, autouse only invokes the fixture, it does not
# make it available in the test's scope


import pytest

import os

this_file_path =  os.path.abspath(__file__)
this_directory = os.path.dirname(this_file_path)

output_file_1 = os.path.join(this_directory,'auto_used_fixture_defined_in_conftest.txt')

fixture_output = 1

def test_automatically_marked_but_not_called_1():
    assert(os.path.isfile(output_file_1), True)

    with pytest.raises(NameError):
        assert(cleandir['output']== fixture_output)
    with pytest.raises(NameError):
        assert(create_file['output'] == fixture_output)

def test_automatically_marked_and_called_1(create_file):
    assert(os.path.isfile(output_file_1), True)

    with pytest.raises(NameError):
        assert(cleandir['output']== fixture_output)

    assert(create_file['output'] == fixture_output)


