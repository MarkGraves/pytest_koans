# Sometimes test functions do not directly need access to a fixture object.
# in that case you can mark a test item with
#
# @pytest.mark.usefixtures('FIXTURE_FUNCTION_NAME')
#
# Due to the usefixtures marker, the fixture called by name will be required
# for the execution of each test method,
#
# However, you will not have access to its values unless it is passed in
#
# Occasionally, you may want to have fixtures get
# invoked automatically without a usefixtures or funcargs reference.

# Autouse applies at a scope level:
#
# if an autouse fixture is defined in a test module,
# all its test functions automatically use it.
#
# if an autouse fixture is defined in a test class,
# all test methods in the class will use this fixture without a need to
# state it in the test function signature or with a class-level usefixtures decorator.
#
# if an autouse fixture is defined in a conftest.py file then all tests
# in all test modules belows its directory will invoke the fixture.
#
# Notice the terminology, autouse only invokes the fixture, it does not
# make it available in the test's scope

import pytest

import os

this_file_path =  os.path.abspath(__file__)
this_directory = os.path.dirname(this_file_path)

output_file_1 = os.path.join(this_directory,'auto_used_fixture_defined_in_conftest.txt')

fixture_output = 1


@pytest.fixture(autouse=True)
def cleandir():
    if os.path.isfile(output_file_1):
        os.remove(output_file_1)
    return {'output':fixture_output}

@pytest.fixture(autouse=True)
def create_file():
    with open(output_file_1,'w') as file:
        file.write(str(fixture_output))

    with open(output_file_1,'rb') as file:
        fixture_output_1 = int(file.read())
        assert(fixture_output_1 == fixture_output)
    return {'output':fixture_output}