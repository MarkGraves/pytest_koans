# Just some testing files to look at the structure of collection done by py.test

1.) py.test -s from main directory to see print output of the tests (for illustrative purposes of which tests should be run)


2.) py.test --collect-only to see tree

# Test Discovery

py.test --collect-only
--> recurses into directories to find tests from current working directory

py.test --collect-only FILENAME
--> finds tests in FILENAME

## Collection
1. collection starts from the initial command line arguments which may be directories, filenames or test ids.
2. recurse into directories, unless they match norecursedirs (set in pytest.ini, tox.ini, or setup.cfg)
3. finds files prefixed with test_ or ending in _test.py
4. finds Test prefixed test classes (cannot contain an __init__ method)
5. Valid test items are functions prefixed with test_
6. Valid test items are also class methods prefixed with test_


# Fixtures
Per pytest documentation, fixtures are simply paramaterizable function objects
http://pytest.org/latest/_modules/_pytest/python.html#fixture

By returning a value from them, they can be modified and the modifications can
be accessed. (find source for actually calling them to show this)

This access however is only maintained in the scope of a fixture.


http://pytest.org/latest/_modules/_pytest/python.html#fixture
py.test --fixtures FILENAME
--> get fixtures in file
py.test --fixtures
--> get fixtures in files collected by pytest in current working directory (does not recurse)
